class Ingredient:
    def __init__(self, name: str, price: int, description: str):
        self.Name = name
        self.Price = price
        self.Description = description


class Pizza:
    def __init__(self, name: str):
        self.Name = name #nome pizza
        self.ingredients= [] #ingredienti pizza

    def addIngredient(self, ingredient: Ingredient):
        for i in range(0, len(self.ingredients)):
            if(self.ingredients[i].Name==ingredient.Name):
              raise Exception("Ingredient already inserted") #controllo che l'ingrediente non ci sia già
        self.ingredients.append(ingredient) #aggiungo l'ingrediente alla lista sotto forma di Ingredient

    def numIngredients(self) -> int:
        return len(self.ingredients) #calcolo quanti ingredienti ho

    def computePrice(self) -> int:
        prezzo=0
        for i in range(len(self.ingredients)):
            prezzo += self.ingredients[i].Price #sommo prezzo tutti ingredienti
        return prezzo



class Order:
    def __init__(self):
        self.numorder = -1
        self.listpizze = [] #creo una lista di pizze

    def getPizza(self, position: int) -> Pizza:
        if(position>len(self.listpizze) or position < 0 ): #controllo che la posizione esista
            raise Exception("Position passed is wrong")
        else:
            return self.listpizze[position-1] #-1 perché l'indice parte da 0

    def initializeOrder(self, numPizzas: int):
        if numPizzas < 1: #controllo che l'ordine non sia vuoto
            raise Exception ("Empty order")
        self.listpizze=[]*numPizzas #creo uno spazio quanto sono il numero di pizze

    def addPizza(self, pizza: Pizza):
        self.listpizze.append(pizza) #aggiungo pizza all'ordine

    def numPizzas(self) -> int:
        return len(self.listpizze)

    def computeTotal(self) -> int:
        prezzo= 0
        for i in range(len(self.listpizze)):
            prezzo += self.listpizze[i].computePrice()
        return prezzo





class Pizzeria:

    def __init__(self):
        self.ingredienti = []
        self.pizze = []
        self.ordini = []

    def addIngredient(self, name: str, description: str, price: int):
        for i in range(len(self.ingredienti)):
            if(name==self.ingredienti[i].Name): #controllo che esista gia l'ingrediente
                raise Exception("Ingredient already inserted")
        self.ingredienti.append(Ingredient(name, price, description)) #lo aggiungo tramite append


    def findIngredient(self, name: str) -> Ingredient:
        i = 0
        trovato = False
        for i in range(len(self.ingredienti)):
            if(self.ingredienti[i].Name==name):
                trovato= True
                break
        if(trovato==False ):
            raise Exception("Ingredient not found")
        else:
            return self.ingredienti[i]


    def addPizza(self, name: str, ingredients: []):
        trovato = False
        for i in range(0, len(self.pizze)):
            if self.pizze[i].Name == name:#controllo se la pizza esiste già
                trovato = True
                raise Exception("Pizza already inserted")
        if trovato == False : #non esiste, la aggiungo
            pizza: Pizza = Pizza("")
            pizza.Name = name
            for j in ingredients : #è una lista, quindi per ogni stringa i in ingredients
                                   # mi trova l'ingrediente e lo mette dentro gli ingredienti della pizza
                pizza.ingredients.append(self.findIngredient(j))
                #dopo che trovo  l'ingrediente con il nome usando la funzione find ingredient,
                # lo aggiungo alla pizza usando append
            self.pizze.append(pizza) #aggiungo la nuova pizza

    def findPizza(self, name: str) -> Pizza:
        trovato = False
        i = 0
        for i in range(0, len(self.pizze)):
            if(self.pizze[i].Name==name):
                trovato = True
                break

        if(trovato == False):
            raise Exception("Pizza not found")
        else:
            return self.pizze[i]


    def createOrder(self, pizzas: []) -> int:
        order: Order = Order()
        order.initializeOrder(len(pizzas))
        for i in range(0, len(pizzas)):
            order.listpizze.append(self.findPizza(pizzas[i]))
        order.numorder = len(self.ordini)+1000 #aggiungo 1000 perche partono da lì gli ordini
        self.ordini.append(order)
        return order.numorder


    def findOrder(self, numOrder: int) -> Order:
        if(numOrder > 999+len(self.ordini)) or numOrder < 1000:  # controllo che il numero dell'ordine sia maggiore
                                                                 # di mille e che l'ordine esista, cioè che il
                                                                 # numero dell'ordine non superi il numero totale
                                                                 # degli ordini
            raise Exception("Order not found")
        else:
             return self.ordini[numOrder-1000]


    def getReceipt(self, numOrder: int) -> str:
        ordine = self.findOrder(numOrder)
        result = "" #creo stringa per stampare scontrino
        for i in range(ordine.numPizzas()):
            result = result + "- " + ordine.listpizze[i].Name + ", " + str(ordine.listpizze[i].computePrice()) + " euro" + "\n" #formato stampa richiesto
        result += "  TOTAL: " + str(ordine.computeTotal()) + " euro" + "\n"
        return result




    def listIngredients(self) -> str:
        result=""
        nomi = []
        for k in range(0, len(self.ingredienti)):
            nomi.append(self.ingredienti[k].Name)
        nomi.sort() #mette in ordine alfabetico gli ingredienti

        for j in range(0, len(nomi)):
            result += nomi[j] + " - '" + self.findIngredient(nomi[j]).Description + "': " + str(self.findIngredient(nomi[j]).Price) + " euro" + "\n"
        return result


    def menu(self) -> str:
        result = ""
        for i in range(0, len(self.pizze)):
            result += self.pizze[i].Name + " (" + str(self.pizze[i].numIngredients()) + " ingredients): " + str(self.pizze[i].computePrice()) + " euro" + "\n"
        return result