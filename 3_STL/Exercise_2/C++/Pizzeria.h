#ifndef PIZZERIA_H
#define PIZZERIA_H

#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

namespace PizzeriaLibrary {

  class Ingredient {
    public:
      string Name;
      int Price;
      string Description;
      bool operator<(const Ingredient &ingredient) const;
  };

  class Pizza {
    public:
     string Name;
     vector<Ingredient> ingredients; //inserisco un vettore di ingredienti
      void AddIngredient(const Ingredient& ingredient);
      int NumIngredients() const; //numero ingredienti della pizza
      int ComputePrice() const; //somma prezzo ingredienti
  };

  class Order {
    public:
      vector<Pizza> pizzas; // vettore che ha le pizze ordinate
      void InitializeOrder(int numPizzas);  //creo uno spazio in memoria quanto il numero delle pizze
      void AddPizza(const Pizza& pizza);
      const Pizza& GetPizza(const int& position) const;
      int NumPizzas() const; // da il numero di pizze ordinate
      int ComputeTotal() const; // prezzo ordine
  };

  class Pizzeria  {
    public:
    vector<Ingredient> ingredients; // ingredienti della pizzeria
    vector<Pizza> pizzas; // pizze nel menú
    vector<Order> orders; // ordini
    int numOrders= 1000;
     void AddIngredient(const string& name,
                         const string& description,
                         const int& price);

      const Ingredient& FindIngredient(const string& name) const;
      void AddPizza(const string& name,
                    const vector<string>& ingredients);
      const Pizza& FindPizza(const string& name) const;
      int CreateOrder(const vector<string>& pizzas);
      const Order& FindOrder(const int& numOrder) const;
      string GetReceipt(const int& numOrder) const;
      string ListIngredients() const;
      string Menu() const;
  };
};

#endif // PIZZERIA_H
