#include "Pizzeria.h"

namespace PizzeriaLibrary {

//CLASSE INGREDIENT
bool Ingredient::operator<(const Ingredient &ingredient) const
{
    return (Name<ingredient.Name); //overload< per l'ordine alfabetico degli ingredienti per usare la funzione sort
}

//CLASSE PIZZA
void Pizza::AddIngredient(const Ingredient &ingredient) //aggiungo un ingrediente alla pizza
{
    unsigned int contatore=0;
    for(int i=0;i<NumIngredients();i++)
    {
        if(ingredient.Name == ingredients[i].Name)
        {
          contatore++; //utilizzo per vedere se ingrediente è giá presente
        }
        if(contatore>2)
                throw runtime_error("Ingredient already inserted"); //non posso mettere più di due volte lo stesso ingrediente
    }
    ingredients.push_back(ingredient);
}

int Pizza::NumIngredients() const { return ingredients.size(); }//numero di ingredienti della pizza


int Pizza::ComputePrice() const
{
    int prezzo=0;
    for(int i=0; i<NumIngredients();i++)
    {
        prezzo = prezzo + ingredients[i].Price; //sommo il prezzo di tutti gli ingredienti
    }
    return prezzo; //prezzo della pizza dato dalla somma dei prezzi degli ingredienti
}

//CLASSE ORDER
void Order::InitializeOrder(int numPizzas) { pizzas.reserve(numPizzas); }  //creo uno spazio in memoria quanto il numero delle pizze per l'ordine

void Order::AddPizza(const Pizza &pizza) { pizzas.push_back(pizza); } //aggiungo la pizza all'ordine

const Pizza& Order::GetPizza(const int &position) const
{
    if(position<0 || position>NumPizzas()) //controllo che la posizione esista
        {
           throw runtime_error("Position passed is wrong");
        }
    return pizzas[position]; //prendo la pizza nella posizione cercata
}

int Order::NumPizzas() const { return pizzas.size(); } //numero di pizze nell'ordine

int Order::ComputeTotal() const
{
    int prezzo=0;
    for(int i=0; i<NumPizzas();i++)
    {
        prezzo += pizzas[i].ComputePrice(); //sommo il prezzo di tutte le pizze
    }
    return prezzo;
}


//CLASSE PIZZERIA
void Pizzeria::AddIngredient(const string &name, const string &description, const int &price) //aggiunge ingredienti
{
    bool trovato = false;
    Ingredient ingrediente;
    ingrediente.Name= name;
    ingrediente.Description= description;
    ingrediente.Price= price;
    // settato l'ingrediente
    for(unsigned int i=0;i<ingredients.size(); i++) // ingredients è quello della cucina, cerchiamo se è presente
    {
        if (ingrediente.Name==ingredients[i].Name) //vedo se l'ingrediente esiste già
        {
            trovato = true;
            break;
        }
    }
    if(trovato == true)
    {
        throw runtime_error("Ingredient already inserted");
    }

    if(trovato == false) //se non esiste lo aggiungo
    {
        ingredients.push_back(ingrediente);
    }
    sort(ingredients.begin(),ingredients.end()); //ordino in ordine alfabetico, per defaul mette in ordine crescente = alfabetico
}

const Ingredient &Pizzeria::FindIngredient(const string &name) const
{
    bool trovato = false;
    int j=0;
    for(unsigned int i=0; i< ingredients.size();i++) //cerchiamo l'elemento
    {
        if(ingredients[i].Name == name)
        {
            j=i; //salvo la posizione dell'ingrediente per ritornarlo
            trovato = true;
            break;
        }
    }
    if(trovato == true)
    {
        return ingredients[j]; // ritorno l'ingrediente nella posizione j.
    }
    else
    {
        throw runtime_error ("Ingredient not found");
    }
}

void Pizzeria::AddPizza(const string &name, const vector<string> &ingredients)
{
    bool trovato= false;
    for(unsigned int i=0;i<pizzas.size(); i++)
    {
        if (name==pizzas[i].Name) //vedo se la pizza esiste già
        {
            trovato= true;
            break;
        }

    }
    if(trovato == true) // eccezione se è giá presente
    {
        throw runtime_error("Pizza already inserted");
    }
    if(trovato == false) //se non esiste la aggiungo
    {
        pizzas.resize(pizzas.size()+1); //aumento lo spazio riservato alle pizze
        pizzas[pizzas.size()-1].Name = name;
        for( unsigned int i =0; i< ingredients.size(); i++)
        {
            pizzas[pizzas.size()-1].AddIngredient(FindIngredient(ingredients[i])); // trovo l'ingrediente e lo aggiungo alla pizza
            //-1 perchè l'indice parte da 0
        }
    }

}

const Pizza &Pizzeria::FindPizza(const string &name) const
{
    bool trovato = false;
    int j=0;
    for(unsigned int i=0; i< pizzas.size();i++) //cerchiamo l'elemento
    {
        if(pizzas[i].Name == name)
        {
            j=i; //salvo la posizione per ritornarlo
            trovato = true;
            break;
        }
    }
    if(trovato== true)
    {
        return pizzas[j];
    }
    else
    {
        throw runtime_error ("Pizza not found");
    }
}

int Pizzeria::CreateOrder(const vector<string> &pizzas)
{
    Order ordine;
    if(pizzas.size() <= 0)
    {
        throw runtime_error("Empty order");
    }
    else
    {
        Pizza pizza;
        ordine.InitializeOrder(pizzas.size()); //inizializzo l'ordine
        for(unsigned int i=0; i<pizzas.size();i++)
        {
            pizza = FindPizza(pizzas[i]); //creo una variabile di appoggio dove ci pongo la pizza i-esima di pizzas (menú) e la trasformo come la classe pizza
            ordine.AddPizza(pizza); //aggiungo la pizza all'ordine
        }
        orders.push_back(ordine); //aggiungo l'ordine alla lista degli ordini
        numOrders++; //aggiungo uno per incrementare il numero di ordine
        return numOrders-1; //-1 perchè ritono l'ordine appena fatto e non quello incrementato
    }
}

const Order &Pizzeria::FindOrder(const int &numOrder) const
{
    if(numOrder<1000 || numOrder>numOrders-1) //controlliamo che sia maggiore di 1000 e anche che l'ordine esista, cioè che il numero non sia maggiore degli numeri di ordini esistente
    {
        throw runtime_error("Order not found");
    }
    else
    {
        return orders[numOrder-1000]; //ritorno l'ordine che nel nostro vettore parte dal numero che ci danno -1000 perchè orders parte da 0 in quanto vettore
    }
}

string Pizzeria::GetReceipt(const int &numOrder) const
{
    Order ordine= FindOrder(numOrder); //prendo una variabile e ci metto dentro l'ordine
    string scontrino= "";
    string prezzo,totale;
    for(int i=0; i<ordine.NumPizzas();i++)
    {
        Pizza pizza = ordine.GetPizza(i); //prendo le pizze dall'ordine;
        prezzo = to_string(pizza.ComputePrice()); //to string mi trasforma il prezzo da intero a stringa
        scontrino +="- "+pizza.Name+", "+prezzo+" euro"+"\n"; //+= perchè ad ogni for concateno le stringhe

    }
    totale = to_string(ordine.ComputeTotal()); //converto con to string da int a stringa
    scontrino+="  TOTAL: " + totale + " euro" + "\n"; //aggiungo il totale allo scontrino
    return scontrino;
}

string Pizzeria::ListIngredients() const
{
    string stampa,prezzo;
    stampa="";
    for(unsigned int i=0; i<ingredients.size();i++)
    {
        prezzo = to_string( ingredients[i].Price); //converto intero a stringa
        stampa += ingredients[i].Name+" - '"+ingredients[i].Description+"': "+prezzo+ " euro"+"\n";
        //+= perchè ad ogni giro concateno gli ingredienti per fare una lista sola
    }
    return stampa;

}

string Pizzeria::Menu() const
{
    string menu="";
    string numIng="";
    string prezzo="";
    for(int i=(pizzas.size()-1); i>=0; i--)
    {
        numIng= to_string(pizzas[i].NumIngredients()); // con to string converto un intero in stringa
        prezzo= to_string(pizzas[i].ComputePrice());// con to string converto un intero in stringa
        menu += pizzas[i].Name + " (" + numIng + " ingredients): " + prezzo + " euro" + "\n"; //+= per concatenare le stringhe

    }
    return menu;
}



}
