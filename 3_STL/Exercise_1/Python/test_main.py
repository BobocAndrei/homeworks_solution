from unittest import TestCase

import random
import string
import time
import main as shoppingLibrary

numElements = 100000

def randomString(length : int) -> str:
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))

def fillList(application : shoppingLibrary.IShoppingApp, numberElement : int):
    for i in range(0, numberElement):
        application.addElement(randomString(10))

class TestShoppingApp(TestCase):
    def test_vector(self):
        application = shoppingLibrary.VectorShoppingApp()
        try:
            start = time.time()
            fillList(application, numElements)
            stop = time.time()
            elapsed = (stop - start) * 1000
            print('{} ({}ms)'.format("Insert:", round(elapsed, 3)))
        except:
            self.fail()

        try:
            start = time.time()
            application.undo()
            stop = time.time()
            elapsed = (stop - start) * 1000
            print('{} ({}ms)'.format("Undo:", round(elapsed, 3)))
        except:
            self.fail()

        try:
            start = time.time()
            application.searchElement("sadaskjbdk")
            stop = time.time()
            elapsed = (stop - start) * 1000
            print('{} ({}ms)'.format("Search:", round(elapsed, 3)))
        except:
            self.fail()

    def test_list(self):
        application = shoppingLibrary.ListShoppingApp()
        try:
            start = time.time()
            fillList(application, numElements)
            stop = time.time()
            elapsed = (stop - start) * 1000
            print('{} ({}ms)'.format("Insert:", round(elapsed, 3)))
        except:
            self.fail()

        try:
            start = time.time()
            application.undo()
            stop = time.time()
            elapsed = (stop - start) * 1000
            print('{} ({}ms)'.format("Undo:", round(elapsed, 3)))
        except:
            self.fail()

        try:
            start = time.time()
            application.searchElement("sadaskjbdk")
            stop = time.time()
            elapsed = (stop - start) * 1000
            print('{} ({}ms)'.format("Search:", round(elapsed, 3)))
        except:
            self.fail()

    def test_stack(self):
        application = shoppingLibrary.StackShoppingApp()
        try:
            start = time.time()
            fillList(application, numElements)
            stop = time.time()
            elapsed = (stop - start) * 1000
            print('{} ({}ms)'.format("Insert:", round(elapsed, 3)))
        except:
            self.fail()

        try:
            start = time.time()
            application.undo()
            stop = time.time()
            elapsed = (stop - start) * 1000
            print('{} ({}ms)'.format("Undo:", round(elapsed, 3)))
        except:
            self.fail()

        try:
            start = time.time()
            application.searchElement("sadaskjbdk")
            stop = time.time()
            elapsed = (stop - start) * 1000
            print('{} ({}ms)'.format("Search:", round(elapsed, 3)))
        except:
            self.fail()

    def test_queue(self):
        application = shoppingLibrary.QueueShoppingApp()
        try:
            start = time.time()
            fillList(application, numElements)
            stop = time.time()
            elapsed = (stop - start) * 1000
            print('{} ({}ms)'.format("Insert:", round(elapsed, 3)))
        except:
            self.fail()

        try:
            start = time.time()
            application.undo()
            stop = time.time()
            elapsed = (stop - start) * 1000
            print('{} ({}ms)'.format("Undo:", round(elapsed, 3)))
        except:
            self.fail()

        try:
            start = time.time()
            application.searchElement("sadaskjbdk")
            stop = time.time()
            elapsed = (stop - start) * 1000
            print('{} ({}ms)'.format("Search:", round(elapsed, 3)))
        except:
            self.fail()

    def test_hash(self):
        application = shoppingLibrary.HashShoppingApp()
        try:
            start = time.time()
            fillList(application, numElements)
            stop = time.time()
            elapsed = (stop - start) * 1000
            print('{} ({}ms)'.format("Insert:", round(elapsed, 3)))
        except:
            self.fail()

        try:
            start = time.time()
            application.undo()
            stop = time.time()
            elapsed = (stop - start) * 1000
            print('{} ({}ms)'.format("Undo:", round(elapsed, 3)))
        except:
            self.fail()

        try:
            start = time.time()
            application.searchElement("sadaskjbdk")
            stop = time.time()
            elapsed = (stop - start) * 1000
            print('{} ({}ms)'.format("Search:", round(elapsed, 3)))
        except:
            self.fail()
