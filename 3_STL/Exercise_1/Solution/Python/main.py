class IShoppingApp:
    def numberElements(self) -> int:
        pass

    def addElement(self, product: str):
        pass

    def undo(self):
        pass

    def reset(self):
        pass

    def searchElement(self, product: str) -> bool:
        return False


class VectorShoppingApp(IShoppingApp):
    def __init__(self):
        self.__elements = []

    def numberElements(self) -> int:
        return len(self.__elements)

    def addElement(self, product: str):
        self.__elements.append(product)

    def undo(self):
        self.__elements.pop()

    def reset(self):
        self.__elements.clear()

    def searchElement(self, product: str) -> bool:
        # return product in self.__elements
        numElements = self.numberElements()
        found = False
        for i in range(0, numElements):
            if self.__elements[i] == product:
                found = True
                break
        return found


class ListShoppingApp(IShoppingApp):
    def __init__(self):
        self.__elements = []

    def numberElements(self) -> int:
        return len(self.__elements)

    def addElement(self, product: str):
        self.__elements.append(product)

    def undo(self):
        self.__elements.pop()

    def reset(self):
        self.__elements.clear()

    def searchElement(self, product: str) -> bool:
        # return product in self.__elements
        found = False
        for element in self.__elements:
            if element == product:
                found = True
                break
        return found


class StackShoppingApp(IShoppingApp):
    def __init__(self):
        self.__elements = []

    def numberElements(self) -> int:
        return len(self.__elements)

    def addElement(self, product: str):
        self.__elements.append(product)

    def undo(self):
        self.__elements.pop()

    def reset(self):
        self.__elements.clear()

    def searchElement(self, product: str) -> bool:
        searchElements = []
        numElements = self.numberElements()
        found = False
        for i in range(0, numElements):
            element = self.__elements.pop()
            searchElements.append(element)
            if element == product:
                found = True
                break
        numFoundElements = len(searchElements)
        for i in range(0, numFoundElements):
            self.__elements.append(searchElements.pop())
        return found


class QueueShoppingApp(IShoppingApp):
    def __init__(self):
        self.__elements = []

    def numberElements(self) -> int:
        return len(self.__elements)

    def addElement(self, product: str):
        self.__elements.append(product)

    def undo(self):
        undoElements = []
        numElements = self.numberElements()
        for i in range(0, numElements - 1):
            undoElements.append(self.__elements.pop(0))
        self.__elements.pop(0)
        for i in range(0, numElements - 1):
            self.__elements.append(undoElements.pop(0))

    def reset(self):
        self.__elements.clear()

    def searchElement(self, product: str) -> bool:
        searchElements = []
        numElements = self.numberElements()
        found = False
        for i in range(0, numElements):
            element = self.__elements.pop(0)
            searchElements.append(element)
            if element == product:
                found = True
                break
        numFoundElements = len(searchElements)
        for i in range(0, numFoundElements):
            self.__elements.append(searchElements.pop(0))
        return found


class HashShoppingApp(IShoppingApp):
    def __init__(self):
        self.__elements = set()

    def numberElements(self) -> int:
        return len(self.__elements)

    def addElement(self, product: str):
        self.__elements.add(product)

    def undo(self):
        self.__elements.pop()

    def reset(self):
        self.__elements.clear()

    def searchElement(self, product: str) -> bool:
        return product in self.__elements
