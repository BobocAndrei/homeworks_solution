import math

class Point:
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y


class IPolygon:
    def area(self) -> float:
        return 0.0


class Ellipse(IPolygon):
    def __init__(self, center: Point, a: int, b: int):
        self.center = center
        self.a = a
        self.b = b

    def area(self) -> float:
        return self.a*self.b*math.pi


class Circle(IPolygon):
    def __init__(self, center: Point, radius: int):
        self.center = center
        self.radius = radius

    def area(self) -> float:
        return math.pi*self.radius*self.radius


class Triangle(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point):
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3

    def area(self) -> float:
        l12 = math.sqrt(pow(self.p1.x - self.p2.x, 2) + pow(self.p1.y - self.p2.y, 2))
        l23 = math.sqrt(pow(self.p2.x - self.p3.x, 2) + pow(self.p2.y - self.p3.y, 2))
        l31 = math.sqrt(pow(self.p3.x - self.p1.x, 2) + pow(self.p3.y - self.p1.y, 2))
        p = (l12 + l23 + l31)/2
        return math.sqrt(p * (p - l12) * (p - l23) * (p - l31))


class TriangleEquilateral(IPolygon):
    def __init__(self, p1: Point, edge: int):
        self.p1 = p1
        self.edge = edge

    def area(self) -> float:
        return (math.sqrt(3))*0.5*self.edge*self.edge * 0.5


class Quadrilateral(IPolygon):
    def __init__(self, p1: Point, p2: Point, p3: Point, p4: Point):
        self.p1 = p1
        self.p2 = p2
        self.p3 = p3
        self.p4 = p4

    def area(self) -> float:
        return 0.5*abs((self.p1.x*self.p2.y)+(self.p2.x*self.p3.y)+(self.p3.x*self.p4.y)+(self.p4.x*self.p1.y)-(self.p1.y*self.p2.x)-(self.p2.y*self.p3.x)-(self.p3.y*self.p4.x)-(self.p4.y*self.p1.x))


class Parallelogram(IPolygon):
    def __init__(self, p1: Point, p2: Point, p4: Point):
        self.p1 = p1
        self.p2 = p2
        self.p4 = p4

    def area(self) -> float:
        return Quadrilateral(self.p1, self.p2, Point(self.p4.x - self.p1.x + self.p2.x, self.p4.y), self.p4).area()


class Rectangle(IPolygon):
    def __init__(self, p1: Point, base: int, height: int):
        self.p1 = p1
        self.base = base
        self.height = height

    def area(self) -> float:
        return Quadrilateral(self.p1, Point(self.p1.x+self.base, self.p1.y), Point(self.p1.x+self.base, self.p1.y + self.height), Point(self.p1.x, self.p1.y + self.height)).area()


class Square(IPolygon):
    def __init__(self, p1: Point, edge: int):
        self.p1 = p1
        self.edge = edge

    def area(self) -> float:
        return Quadrilateral(self.p1, Point(self.p1.x+self.edge, self.p1.y), Point(self.p1.x+self.edge, self.p1.y+self.edge), Point(self.p1.x, self.p1.y+self.edge)).area()
