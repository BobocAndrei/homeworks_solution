# Polygon

The computation of polygon area is a fundamental part of Computational Science.

Most interesting polygons are for example:

* Triangle
* Quadrilateral
* Ellipse

## Requirements

Write a software able to compute the area of the polygons listed:

* Triangle
  * Triangle Equilateral
* Quadrilateral
  * Parallelogram
  * Rectangle
  * Square
* Ellipse
  * Circle

The following structure shall be implemented:

![polygon_cd](Images/polygon_cd.png)