#include "shape.h"
#include <math.h>

namespace ShapeLibrary {
Point::Point(const double &x, const double &y)
{
    _x=x;
    _y=y;
}

Point::Point(const Point &point)
{
    _x=point._x;
    _y=point._y;
}


Ellipse::Ellipse(const Point &center, const int &a, const int &b)
{
    _center._x = center._x;
    _center._y = center._y;
    _a=a;
    _b=b;

}

double Ellipse::Area() const
{
    return _a*_b*M_PI; //usiamo la libreria math.h
}


Circle::Circle(const Point &center, const int &radius):Ellipse(center,radius,radius)
{
    _center._x = center._x;
    _center._y = center._y;
    _radius=radius;

}

double Circle::Area() const
{
    return Ellipse::Area(); //dato che il cerchio eredita da ellisse usiamo il suo metodo
}


Triangle::Triangle(const Point &p1, const Point &p2, const Point &p3)
{
    _p1._x=p1._x;
    _p1._y=p1._y;
    _p2._x=p2._x;
    _p2._y=p2._y;
    _p3._x=p3._x;
    _p3._y=p3._y;
}

double Triangle::Area() const
{
    double l12,l23,l31;
    l12=sqrt(pow(_p1._x-_p2._x,2)+pow(_p1._y-_p2._y,2));
    l23=sqrt(pow(_p2._x-_p3._x,2)+pow(_p2._y-_p3._y,2));
    l31=sqrt(pow(_p3._x-_p1._x,2)+pow(_p3._y-_p1._y,2));
    double p;
    p=(l12+l23+l31)/2;
    return(sqrt(p*(p-l12)*(p-l23)*(p-l31))); // formula di Erone
}


TriangleEquilateral::TriangleEquilateral(const Point &p1, const int &edge):Triangle(p1,Point(p1._x+edge,p1._y),Point(p1._x+0.5*edge,p1._y+sqrt(3)*0.5*edge))
{
    _p1._x=p1._x;
    _p1._y=p1._y;
    _edge=edge;
}

double TriangleEquilateral::Area() const
{
    return Triangle::Area(); //eredita il metodo
}


Quadrilateral::Quadrilateral(const Point &p1, const Point &p2, const Point &p3, const Point &p4)
{
    _p1._x=p1._x;
    _p1._y=p1._y;
    _p2._x=p2._x;
    _p2._y=p2._y;
    _p3._x=p3._x;
    _p3._y=p3._y;
    _p4._x=p4._x;
    _p4._y=p4._y;
}

double Quadrilateral::Area() const
{
    double area;
    area=0.5*abs((_p1._x*_p2._y)+(_p2._x*_p3._y)+(_p3._x*_p4._y)+(_p4._x*_p1._y)-(_p1._y*_p2._x)-(_p2._y*_p3._x)-(_p3._y*_p4._x)-(_p4._y*_p1._x));
    return area;
}


Parallelogram::Parallelogram(const Point &p1, const Point &p2, const Point &p4):Quadrilateral(p1,p2,Point(p4._x-p1._x+p2._x,p4._y),p4)
{
    _p1._x=p1._x;
    _p1._y=p1._y;
    _p2._x=p2._x;
    _p2._y=p2._y;
    _p4._x=p4._x;
    _p4._y=p4._y;

}

double Parallelogram::Area() const
{
    return Quadrilateral::Area();
}


Rectangle::Rectangle(const Point &p1, const int &base, const int &height):Quadrilateral(p1,Point(p1._x+base,p1._y),Point(p1._x+base,p1._y+height),Point(p1._x,p1._y+height))
{
    _p1._x=p1._x;
    _p1._y=p1._y;
    _base=base;
    _height=height;
}

double Rectangle::Area() const
{
    return Quadrilateral::Area();
}


Square::Square(const Point &p1, const int &edge):Quadrilateral(p1,Point(p1._x+edge,p1._y),Point(p1._x+edge,p1._y+edge),Point(p1._x,p1._y+edge))
{
    _p1._x=p1._x;
    _p1._y=p1._y;
    _edge=edge;
}

double Square::Area() const
{
    return Quadrilateral::Area();
}


}
