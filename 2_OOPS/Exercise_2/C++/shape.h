#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>

using namespace std;

namespace ShapeLibrary {

  class Point {
    public:
      double _x,_y;
//default constructor
      Point() : _x(0), _y(0) {}
//constructor with default trailing argument
      Point(const double& x,
            const double& y);
//copy constructor
      Point(const Point& point);
  };

  class IPolygon {
    public:
      virtual double Area() const = 0;
  };

  class Ellipse : public IPolygon
  {
    public:
      Point _center;
      int _a,_b;
      Ellipse(const Point& center,
              const int& a,
              const int& b);

      double Area() const;
  };

  class Circle : public Ellipse
  {
    public:
      int _radius;
      Circle(const Point& center,
             const int& radius);

      double Area() const;
  };


  class Triangle : public IPolygon
  {
    public:
      Point _p1;
      Point _p2;
      Point _p3;
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);

      double Area() const;
  };


  class TriangleEquilateral : public Triangle
  {
    public:
      int _edge;
      TriangleEquilateral(const Point& p1,
                          const int& edge);

      double Area() const;
  };

  class Quadrilateral : public IPolygon
  {
    public:
      Point _p1;
      Point _p2;
      Point _p3;
      Point _p4;
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);

      double Area() const;
  };


  class Parallelogram : public Quadrilateral
  {
    public:
      Parallelogram(const Point& p1,
                    const Point& p2,
                    const Point& p4);

      double Area() const;
  };

  class Rectangle : public Quadrilateral
  {
    public:
      int _base,_height;
      Rectangle(const Point& p1,
                const int& base,
                const int& height);

      double Area() const;
  };

  class Square: public Quadrilateral
  {
    public:
      int _edge;
      Square(const Point& p1,
             const int& edge);

      double Area() const;
  };
}

#endif // SHAPE_H
