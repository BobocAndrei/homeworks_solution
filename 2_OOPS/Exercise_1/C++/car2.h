#ifndef CAR2_H
#define CAR2_H

#include <iostream>

using namespace std;

namespace CarLibrary2 {

  class Car
  {
    public:
      Car(const string& producer,
          const string& model,
          const string& color) { }

      string Show() { return ""; }

  };

  class ICarFactory
  {
    public:
      virtual Car* Create(const string& color) = 0;
  };

  class Ford : public ICarFactory
  {
    public:
      Car* Create(const string& color) { return nullptr; }
  };

  class Toyota : public ICarFactory
  {
    public:
      Car* Create(const string& color) { return nullptr; }
  };

  class Volkswagen : public ICarFactory
  {
    public:
      Car* Create(const string& color) { return nullptr; }
  };
}

#endif // CAR2_H
