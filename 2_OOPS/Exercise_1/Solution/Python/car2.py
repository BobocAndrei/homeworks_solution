class Car:
    def __init__(self, producer: str, model: str, color: str):
        self.__color = color
        self.__producer = producer
        self.__model = model

    def show(self) -> str:
        return self.__model + " (" + self.__producer + "): color " + self.__color


class ICarFactory:
    def create(self, color: str) -> Car:
        return None


class Ford(ICarFactory):
    def create(self, color: str) -> Car:
        return Car("Ford", "Mustang", color)


class Toyota(ICarFactory):
    def create(self, color: str) -> Car:
        return Car("Toyota", "Prius", color)


class Volkswagen(ICarFactory):
    def create(self, color: str) -> Car:
        return Car("Volkswagen", "Golf", color)
