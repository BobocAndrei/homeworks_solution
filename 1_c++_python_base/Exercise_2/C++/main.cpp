#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>

using namespace std;

/// \brief ImportText import the text for encryption
/// \param inputFilePath: the input file path
/// \param text: the resulting text
/// \return the result of the operation, true is success, false is error
bool ImportText(const string& inputFilePath,
                string& text);

/// \brief Encrypt encrypt the text
/// \param text: the text to encrypt
/// \param password: the password for encryption
/// \param encryptedText: the resulting encrypted text
/// \return the result of the operation, true is success, false is error
bool Encrypt(const string& text,
             const string& password,
             string& encryptedText);

/// \brief Decrypt decrypt the text
/// \param text: the text to decrypt
/// \param password: the password for decryption
/// \param decryptedText: the resulting decrypted text
/// \return the result of the operation, true is success, false is error
bool Decrypt(const string& text,
             const string& password,
             string& decryptedText);

int main(int argc, char** argv)
{
  if (argc < 2)
  {
    cerr<< "Password shall passed to the program"<< endl;
    return -1;
  }
  string password = argv[1];

  string inputFileName = "./text.txt", text;
  if (!ImportText(inputFileName, text))
  {
    cerr<< "Something goes wrong with import"<< endl;
    return -1;
  }
  else
    cout<< "Import successful: result= "<< text<< endl;

  string encryptedText;
  if (!Encrypt(text, password, encryptedText))
  {
    cerr<< "Something goes wrong with encryption"<< endl;
    return -1;
  }
  else
    cout<< "Encryption successful: result= "<< encryptedText<< endl;

  string decryptedText;
  if (!Decrypt(encryptedText, password, decryptedText) || text != decryptedText)
  {
    cerr<< "Something goes wrong with decryption"<< endl;
    return -1;
  }
  else
    cout<< "Decryption successful: result= "<< decryptedText<< endl;

  return 0;
}

//Da qui partono le funzioni:

bool ImportText(const string& inputFilePath,
                string& text)
{
  ifstream file; //classe che serve per leggere da un file
  file.open(inputFilePath);
  getline(file,text);
  return true;
}

bool Encrypt(const string& text,
             const string& password,
             string& encryptedText)
{
    int lenText = text.length();
    int lenPass = password.length();

    char *txtArray = NULL;
    txtArray = new char[lenText+1]; //+1 per il / finale
    char *pwArray = NULL;
    pwArray = new char[lenText+1];
    char *momentaneo = NULL;
    momentaneo = new char[lenText+1];

    //metto in un array la frase del file
    for(int a = 0; a<lenText+1; a++)
    {
        txtArray[a] = text[a];
    }

    int i = 0;
    int j = 0;

    //metto tante volte quante serve la password in un array
    while(i<lenText+1)
    {
        pwArray[i] = password[j];
        if(j == lenPass)
        {
            j=0;
        }
        i++;
        j++;
    }

    //ora ho due array di caratteri

    for(int b = 0; b<lenText+1; b++)
    {
        momentaneo[b] = int(txtArray[b])+int(pwArray[b]);
    }

    //cast a string da codice ASCII
    encryptedText = string(momentaneo);


    string filepath = "./parola.txt";
    ofstream file;
    file.open(filepath);
    file<<"La password: " << password << endl;
    file<<"Criptaggio: " <<endl;
    for(int s = 0; s<lenText+1; s++)
    {
        file<< encryptedText[s]<< " ";
    }
    file.close();

    delete []txtArray;
    delete []pwArray;
    delete []momentaneo;

  return true;
}

bool Decrypt(const string& text,
             const string& password,
             string& decryptedText)
{

    int lenText = text.length();
    int lenPass = password.length();

    char *txtArray = NULL;
    txtArray = new char[lenText+1];
    char *pwArray = NULL;
    pwArray = new char[lenText+1];
    char *momentaneo = NULL;
    momentaneo = new char[lenText+1];

    for(int a = 0; a<lenText+1; a++)
    {
        txtArray[a] = text[a];
    }

    int i = 0;
    int j = 0;

    while(i<lenText+1)
    {
        pwArray[i] = password[j];
        if(j == lenPass)
        {
            j=0;
        }
        i++;
        j++;
    }

    //Ho copiato da sopra in quanto è uguale come procedura
    //ho i due array di caratteri, ora per il decriptaggio faccio sa sottrazione al posto della addizione

    for(int b = 0; b<lenText+1; b++)
    {
        momentaneo[b] = int(txtArray[b]) - int(pwArray[b]);
    }

    decryptedText = string(momentaneo);

    ofstream file;
    file.open("./decriptaggio.txt");
    file<<"La password : "<<password<<endl;
    file<<"Decriptaggio"<<endl;
    for(int s = 0; s<lenText+1; s++)
    {
        file<< decryptedText[s];
    }

    delete []txtArray;
    delete []pwArray;

    file.close();
  return true;
}
