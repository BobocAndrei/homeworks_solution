import sys

# \brief ImportText import the text for encryption
# \param inputFilePath: the input file path
# \return the result of the operation, true is success, false is error
# \return text: the resulting text


def importText(inputFilePath):
    myfile = open(inputFilePath, 'r')
    lines = myfile.readlines()  # è una lista con riga nel primo posto
    lines[0] = lines[0].rstrip("\n")  # serve a togliere gli \n
    myfile.close()
    return True, lines[0]

# \brief Encrypt encrypt the text
# \param text: the text to encrypt
# \param password: the password for encryption
# \return the result of the operation, true is success, false is error
# \return encryptedText: the resulting encrypted text


def encrypt(text, password):
    l = len(text)
    x = len(password)
    i = 0
    while i < (l-x):  # aggiungo tante volte le lettere della password
        password = password + password[i]
        i = i + 1

    # print(f"len txt = {l}, len pw = {len(password)}") controllo lunghezza
    lista = [password]
    testo = [text]

    fine = [chr( ord(lista[0][i]) + ord(testo[0][i]) ) for i in range(l)]
    criptaggio = ''.join(fine)  # toglie gli spazi

    return True, criptaggio

# \brief Decrypt decrypt the text
# \param text: the text to decrypt
# \param password: the password for decryption
# \return the result of the operation, true is success, false is error
# \return decryptedText: the resulting decrypted text


def decrypt(text, password):
    l = len(text)
    x = len(password)
    i = 0
    while i < (l-x):
        password = password + password[i]
        i = i + 1

    testo = [text]
    pw = [password]

    decrypt = [ chr( ord(testo[0][j]) - ord(pw[0][j]) ) for j in range(l)]

    decriptaggio = ''.join(decrypt)

    return True, decriptaggio


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Password shall passed to the program")
        exit(-1)
    password = sys.argv[1]

    inputFileName = "text.txt"

    [resultImport, text] = importText(inputFileName)
    if not resultImport:
        print("Something goes wrong with import")
        exit(-1)
    else:
        print("Import successful: text =", text)

    [resultEncrypt, encryptedText] = encrypt(text, password)
    if not resultEncrypt:
        print("Something goes wrong with encryption")
        exit(-1)
    else:
        print("Encryption successful: result = ", encryptedText)

    [resultEncrypt, decryptedText] = decrypt(encryptedText, password)
    if not resultEncrypt or text != decryptedText:
        print("Something goes wrong with decryption")
        exit(-1)
    else:
        print("Decryption successful: result = ", decryptedText)
