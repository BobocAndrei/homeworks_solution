#include "shape.h"
#include <math.h>
#include <iostream>
namespace ShapeLibrary {

Point::Point(const double& x, const double& y)
{
    X=x;
    Y=y;
}
Point::Point(const Point& point)
{
    X=point.X;
    Y=point.Y;
}

double Point::ComputeNorm2() const
{
    return(sqrt(pow(X,2)+pow(Y,2)));

}

Point Point::operator+(const Point& point) const
{
    return Point(X+point.X,Y+point.Y);

}

Point Point::operator-(const Point& point) const
{
    return Point(X-point.X,Y-point.Y);

}

Point&Point::operator-=(const Point& point)
{
   X-=point.X;
   Y-=point.Y;
   return *this;
}

Point&Point::operator+=(const Point& point)
{
    X+=point.X;
    Y+=point.Y;
    return *this;

}
ostream& operator<<(ostream& stream, const Point& point)
{
    stream <<"Point: x = " << point.X << " y = " << point.Y << "\n";
    return stream;

}

Ellipse::Ellipse(const Point &center, const double &a, const double &b)
{
    _center.X = center.X;
    _center.Y = center.Y;
    _a=a;
    _b=b;
}

void Ellipse::AddVertex(const Point &point)
{
    _center.X=point.X;
    _center.Y=point.Y;

}

double Ellipse::Perimeter() const
  {
    double r=sqrt(0.5*(pow(_a,2)+pow(_b,2)));
    return 2*M_PI*r;
  }

Circle::Circle(const Point &center, const double &radius):Ellipse(center,radius,radius)
{
    _radius=radius;

}

double Circle::Perimeter() const
{
    double perimeter=0;
    perimeter=Ellipse::Perimeter();
    return perimeter;
}

  Triangle::Triangle(const Point& p1,
                     const Point& p2,
                     const Point& p3)
  {
      points.resize(3);
      points[0].X=p1.X;
      points[0].Y=p1.Y;
      points[1].X=p2.X;
      points[1].Y=p2.Y;
      points[2].X=p3.X;
      points[2].Y=p3.Y;

  }

  void Triangle::AddVertex(const Point &point)
  {
      int n=points.size();
      if(n>=3) {points.clear(); points.push_back(point);}
      else points.push_back(point);
  }

  double Triangle::Perimeter() const
  {
    double perimeter = 0;
    perimeter=(points[0]-points[1]).ComputeNorm2()+(points[1]-points[2]).ComputeNorm2()+(points[2]-points[0]).ComputeNorm2(); //somma distanze
    return perimeter;
  }

  TriangleEquilateral::TriangleEquilateral(const Point& p1, const double& edge):Triangle(p1,Point(p1.X+edge,p1.Y),Point(p1.X+0.5*edge,p1.Y+sqrt(3)*0.5*edge))
  {
      _edge=edge;

  }

  double TriangleEquilateral::Perimeter() const
  {
      return Triangle::Perimeter();
  }



  Quadrilateral::Quadrilateral(const Point& p1,
                               const Point& p2,
                               const Point& p3,
                               const Point& p4)
  {
      points.resize(4);
      points[0].X=p1.X;
      points[0].Y=p1.Y;
      points[1].X=p2.X;
      points[1].Y=p2.Y;
      points[2].X=p3.X;
      points[2].Y=p3.Y;
      points[3].X=p4.X;
      points[3].Y=p4.Y;
  }

  void Quadrilateral::AddVertex(const Point &p)
  {
      int n=points.size();
      if(n>=4) {points.clear(); points.push_back(p);}
      else points.push_back(p);
  }

  double Quadrilateral::Perimeter() const
  {
    double perimeter = 0;
    perimeter=(points[0]-points[1]).ComputeNorm2()+(points[1]-points[2]).ComputeNorm2()+(points[2]-points[3]).ComputeNorm2()+(points[3]-points[0]).ComputeNorm2();
    return perimeter;
  }

  Rectangle::Rectangle(const Point& p1, const double& base, const double& height):Quadrilateral(p1,Point(p1.X+base,p1.Y),Point(p1.X+base,p1.Y+height),Point(p1.X,p1.Y+height))
  {
    _base=base;
    _height=height;
  }

  double Rectangle::Perimeter() const
  {
      return Quadrilateral::Perimeter();
  }

  Square::Square(const Point &p1, const double &edge):Quadrilateral(p1,Point(p1.X+edge,p1.Y),Point(p1.X+edge,p1.Y+edge),Point(p1.X,p1.Y+edge))
  {
      _edge=edge;
  }

  double Square::Perimeter() const
  {
      return Quadrilateral::Perimeter();
  }

bool operator< (const IPolygon& lhs, const IPolygon& rhs)
{
    //to see which figure is smaller than the other
    //we try to compare polygon's perimeters
    if(lhs.Perimeter()<rhs.Perimeter()) {return true;}
    else return false;
}
bool operator> (const IPolygon& lhs, const IPolygon& rhs)
{
    if(lhs.Perimeter()>rhs.Perimeter()) {return true;}
    else return false;
}




}
