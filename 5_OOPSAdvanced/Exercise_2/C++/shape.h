#ifndef SHAPE_H
#define SHAPE_H

#include <iostream>
#include <vector>
#include <math.h>
using namespace std;

namespace ShapeLibrary {

  class Point {
    public:
      double X;
      double Y;
      //default constructor
      Point():X(0),Y(0) {}
      //constructor with default trailing argument
      Point(const double &x,
            const double &y);
      //copy constructor
      Point(const Point& point);
      //method
      double ComputeNorm2() const;
      //operators
      Point operator+(const Point& point) const;
      Point operator-(const Point& point) const;
      Point& operator-=(const Point& point);
      Point& operator+=(const Point& point);
      friend ostream& operator<<(ostream& stream, const Point& point);
  };

  class IPolygon {
    public:
      virtual double Perimeter() const = 0;
      virtual void AddVertex(const Point& point) = 0;
      friend bool operator< (const IPolygon& lhs, const IPolygon& rhs);
      friend bool operator> (const IPolygon& lhs, const IPolygon& rhs);
      friend bool operator<=(const IPolygon& lhs, const IPolygon& rhs);
      friend bool operator>=(const IPolygon& lhs, const IPolygon& rhs);

  };

  class Ellipse : public IPolygon
  {
    protected:
      Point _center;
      double _a;
      double _b;

    public:
      Ellipse() { }
      Ellipse(const Point& center,
              const double& a,
              const double& b);
      virtual ~Ellipse() { }
      void AddVertex(const Point& point);
      double Perimeter() const;
  };

  class Circle : public Ellipse
  {
    public:
      double _radius;
      Circle() { }
      Circle(const Point& center,
             const double& radius);
      virtual ~Circle() { }
      double Perimeter() const;
  };


  class Triangle : public IPolygon
  {
    protected:
      vector<Point> points;

    public:
      Triangle() { }
      Triangle(const Point& p1,
               const Point& p2,
               const Point& p3);

      void AddVertex(const Point& point);
      double Perimeter() const;
  };


  class TriangleEquilateral : public Triangle
  {
    public:
      double _edge;
      TriangleEquilateral(const Point& p1,
                          const double& edge);
      double Perimeter() const;

  };

  class Quadrilateral : public IPolygon
  {
    protected:
      vector<Point> points;

    public:
      Quadrilateral() { }
      Quadrilateral(const Point& p1,
                    const Point& p2,
                    const Point& p3,
                    const Point& p4);
      virtual ~Quadrilateral() { }
      void AddVertex(const Point& p);

      double Perimeter() const;
  };

  class Rectangle : public Quadrilateral
  {
    public:
      double _base, _height;
      Rectangle() { }
      Rectangle(const Point& p1,
                const Point& p2,
                const Point& p3,
                const Point& p4);
      Rectangle(const Point& p1,
                const double& base,
                const double& height);
      virtual ~Rectangle() { }
      double Perimeter() const;
  };

  class Square: public Quadrilateral
  {
    public:
      double _edge;
      Square() { }
      Square(const Point& p1,
             const Point& p2,
             const Point& p3,
             const Point& p4);
      Square(const Point& p1,
             const double& edge);
      virtual ~Square() { }
      double Perimeter() const;
  };
}

#endif // SHAPE_H
