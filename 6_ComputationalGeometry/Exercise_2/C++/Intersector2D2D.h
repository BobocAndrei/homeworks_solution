#ifndef INTERSECTOR2D2D_HPP
#define INTERSECTOR2D2D_HPP

#include "Eigen"
#include "iostream"

using namespace std;
using namespace Eigen;

class Intersector2D2D;

class Intersector2D2D
{
  public:
    enum TypeIntersection
    {
      NoInteresection = 0,
      Coplanar = 1,
      LineIntersection = 2
    };

  protected:
    TypeIntersection intersectionType;

    double toleranceParallelism;
    double toleranceIntersection;

    Vector3d pointLine;//punto di applicazione della retta di intersezione
    Vector3d tangentLine;//direzione della retta di intersezione
    Vector3d normal1; //vettore normale al primo piano
    Vector3d normal2; //vettore normale al secondo piano
    Vector3d rightHandSide; //vettore dei punti di traslazione del piano
    Matrix3d matrixNormalVector; //matrice con le direzioni normali dei piani

  public:
    Intersector2D2D();
    ~Intersector2D2D();

    void SetTolleranceIntersection(const double& _tolerance) { toleranceIntersection = _tolerance; }
    void SetTolleranceParallelism(const double& _tolerance) { toleranceParallelism = _tolerance; }

    const TypeIntersection& IntersectionType() const { return intersectionType; }
    const double& ToleranceIntersection() const { return toleranceIntersection; }
    const double& ToleranceParallelism() const { return toleranceParallelism; }
    const Vector3d& PointLine() const { return pointLine; }
    const Vector3d& TangentLine() const { return tangentLine; }

    void SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation);
    void SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation);
    bool ComputeIntersection();
};

#endif //INTERSECTOR2D2D_HPP

