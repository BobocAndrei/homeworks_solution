#include "Intersector2D1D.h"

// ***************************************************************************
Intersector2D1D::Intersector2D1D()
{
    toleranceIntersection = 1e-07;
    toleranceParallelism = 1e-07;
    intersectionParametricCoordinate = 0.0;
    intersectionType = TypeIntersection::NoInteresection;
}
Intersector2D1D::~Intersector2D1D(){}
// ***************************************************************************
void Intersector2D1D::SetPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    planeNormalPointer = &planeNormal; //puntatore alla normale
    planeTranslationPointer = &planeTranslation;
}
// ***************************************************************************
void Intersector2D1D::SetLine(const Vector3d& lineOrigin, const Vector3d& lineTangent)
{
    lineOriginPointer = &lineOrigin;
    lineTangentPointer = &lineTangent;
}
// ***************************************************************************
bool Intersector2D1D::ComputeIntersection()
{
    bool intersection = false;
    Vector3d PointTranslation = Vector3d(0.0,0.0,*planeTranslationPointer); //punto
    Vector3d diff =  PointTranslation - *lineOriginPointer; //vettore differenza tra il punto della retta e il punto del piano
    if((*lineTangentPointer).dot(*planeNormalPointer) > toleranceIntersection)//vettori t e n non ortogonali                                                                    //-> c'è intersezione
    {
        intersection = true;
        intersectionType = TypeIntersection::PointIntersection;
        double prod1 = diff.dot(*planeNormalPointer);
        double prod2 = (*lineTangentPointer).dot(*planeNormalPointer);
        double prod3 = prod1 / prod2;
        intersectionParametricCoordinate = prod3;
    }
    else
    {
        if(diff.dot(*planeNormalPointer) != 0)//prodotto scalare tra il vettore differenza tra i due punti e la normale
                                              //del piano
        {
            intersectionType = TypeIntersection::NoInteresection;
            intersection = false;
        }
        else
        {
            intersectionType = TypeIntersection::Coplanar;
            intersection = false;
        }
    }
    return intersection;
}
