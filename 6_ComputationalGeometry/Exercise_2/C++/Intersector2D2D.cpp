#include "Intersector2D2D.h"
using namespace Eigen;
// ***************************************************************************
Intersector2D2D::Intersector2D2D()
{
    toleranceIntersection = 1e-07;
    toleranceParallelism = 1e-05;
    intersectionType = TypeIntersection::NoInteresection;
}


Intersector2D2D::~Intersector2D2D(){}
// ***************************************************************************
void Intersector2D2D::SetFirstPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNormalVector.row(0) = planeNormal;
    normal1 = planeNormal;
    rightHandSide(0) = planeTranslation;
}

// ***************************************************************************
void Intersector2D2D::SetSecondPlane(const Vector3d& planeNormal, const double& planeTranslation)
{
    matrixNormalVector.row(1) = planeNormal;
    normal2 = planeNormal;
    rightHandSide(1) = planeTranslation;
}

// ***************************************************************************
bool Intersector2D2D::ComputeIntersection()
{
    bool intersection = false;
    rightHandSide(2) = 0.0;
    Vector3d normal3 = normal1.cross(normal2);//la direzione della retta è data dal prodotto vettoriale tra le due normali
    if(normal3.norm() > toleranceIntersection) //controllo che le normali non siano ortogonali
    {
        intersection = true;
        intersectionType = TypeIntersection::LineIntersection;
        tangentLine = normal3;
        matrixNormalVector.row(2) = tangentLine;
        pointLine = matrixNormalVector.fullPivLu().solve(rightHandSide);//trovo il punto di applicazione della retta
                                                                        //risolvendo il sistema lineare
    }
    else
    {
       if(abs(rightHandSide(0) - rightHandSide(1)) < toleranceParallelism) //controllo se i piani sono coincidenti
       {
           intersection = false;
           intersectionType = TypeIntersection::Coplanar;
       }
       else
       {
           intersection = false;
       }
    }
    return intersection;
}
